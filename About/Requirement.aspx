﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReportsPage.master" AutoEventWireup="true" CodeBehind="Requirement.aspx.cs" Inherits="ActiveReportsDemo.About.Requirement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
    <table width="900px">
        <tr>
            <td rowspan="3" style="width:300px;">
                <img src="http://www.gcpowertools.com.cn/newimages/step1.png" /></td>
            <td style="font-size: 1.6em;">报表选型顾问</td>
        </tr>
        <tr>
            <td>如果您的项目有报表输出、数据统计、交互式数据分析等需要，我们的专业选型顾问可以免费为您提供需求梳理、产品推荐、视频选型会议讲解、需求验证指导、产品技术支持等多项服务，帮助您快速了解合适的报表方案，高效完成产品评估。</td>
        </tr>
        <tr>
            <td><a href="http://www.gcpowertools.com.cn/products/applyonline.aspx?utm_source=Demo&utm_medium=activereports&utm_term=controlexplorer&utm_content=consultant&utm_campaign=asp.net" target="_blank">立即联系选项顾问</a></td>
        </tr>
        <tr style="height:40px;"><td></td></tr>
        <tr>
            <td rowspan="3">
                <img src="http://www.gcpowertools.com.cn/newimages/step3.png" /></td>
            <td style="font-size: 1.6em;">功能和资源</td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>如果您在浏览该系统时，没有找到您需要的报表，那就将您希望实现报表原型发送给我们吧，我们的技术工程师协助您实现！</li>
                    <li>如果您在浏览该系统时，发现我们提供的实现步骤、视频讲解不够全面，那就将您的想法告诉我们吧，我们的技术工程师会进行完善！</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><a href="http://gcdn.gcpowertools.com.cn/showforum-170.aspx?utm_source=Demo&utm_medium=activereports&utm_term=controlexplorer&utm_content=requirement&utm_campaign=asp.net" target="_blank">立即发送我的需求</a></td>
        </tr>

    </table>
</asp:Content>
