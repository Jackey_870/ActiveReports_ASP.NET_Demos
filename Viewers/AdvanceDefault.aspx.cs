﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActiveReportsDemo.Viewers
{
    public partial class AdvanceDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GrapeCity.ActiveReports.PageReport report = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath(@"~\Reports\rptInvoice3.rdlx")));

            // 获取用户输入数据，并传递到报表中。
            int row = ((GridViewRow)((Button)sender).NamingContainer).RowIndex;
            string parameter1 = Convert.ToString(GridView1.Rows[row].Cells[0].Text);
            report.Report.ReportParameters[0].DefaultValue.Values.Add(parameter1);

            WebViewer1.Report = report;
            WebViewer1.FlashViewerOptions.PrintOptions.StartPrint = false;
            WebViewer1.Visible = true;
            WebViewer1.Width = new Unit(100, UnitType.Percentage);
            WebViewer1.Height = new Unit(100, UnitType.Percentage);

            Panel1.BorderWidth = 2;

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            GrapeCity.ActiveReports.PageReport report = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath(@"~\Reports\rptInvoice3.rdlx")));

            // 获取用户输入数据，并传递到报表中。
            int row = ((GridViewRow)((Button)sender).NamingContainer).RowIndex;
            string parameter1 = Convert.ToString(GridView1.Rows[row].Cells[0].Text);
            report.Report.ReportParameters[0].DefaultValue.Values.Add(parameter1);
            WebViewer1.Report = report;
            WebViewer1.FlashViewerOptions.PrintOptions.StartPrint = true;
            WebViewer1.Visible = true;
            WebViewer1.Width = new Unit(100, UnitType.Percentage);
            WebViewer1.Height = new Unit(100, UnitType.Percentage);
            Panel1.BorderWidth = 0;
        }

        protected int GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            return GridView1.SelectedIndex;
        }
    }
}