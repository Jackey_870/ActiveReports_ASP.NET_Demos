﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="Export.aspx.cs" Inherits="ActiveReportsDemo.Viewers.Export" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
        <asp:Panel ID="Panel1" runat="server" Height="50px">
            <asp:Button ID="btnExcel" runat="server" Text="Excel" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnExcel_Click" />
            <asp:Button ID="btnWord" runat="server" Text="Word" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnWord_Click" />
            <asp:Button ID="btnPdf" runat="server" Text="PDF" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnPdf_Click" />
            <asp:Button ID="btnHtml" runat="server" Text="HTML" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnHtml_Click" />
            <asp:Button ID="btnText" runat="server" Text="Text" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnText_Click" />
            <asp:Button ID="btnCSV" runat="server" Text="CSV" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnCSV_Click" />
            <asp:Button ID="btnImage" runat="server" Text="Image" UseSubmitBehavior="False" CssClass="options_button"
                OnClick="btnImage_Click" />
        </asp:Panel>
        <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="600px" Width="100%"
        ViewerType="FlashViewer" ReportName="">
        <FlashViewerOptions MultiPageViewColumns="1" MultiPageViewRows="1">
        </FlashViewerOptions>
    </ActiveReportsWeb:WebViewer>
</asp:Content>