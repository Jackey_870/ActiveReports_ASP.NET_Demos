﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="TocPanel.aspx.cs" Inherits="ActiveReportsDemo.Viewers.TocPanel" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="600px" Width="100%"
        ViewerType="FlashViewer">
        <FlashViewerOptions MultiPageViewColumns="1" MultiPageViewRows="1">
            <PrintOptions AdjustPaperOrientation="None" ScalePages="None" />
            <TocPanelOptions Visible="True" Width="120" />
        </FlashViewerOptions>
    </ActiveReportsWeb:WebViewer>
</asp:Content>