﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="Theme.aspx.cs" Inherits="ActiveReportsDemo.Viewers.Theme" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
    <asp:Panel ID="Panel1" runat="server" Height="50px" class="options">
        <table>
            <tr>
                <td rowspan="2">请选择皮肤：
                </td>
                <td style="width: 80px; text-align: center;">

                    <asp:RadioButton ID="RadioButton1" GroupName="Theme" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged"
                        AutoPostBack="true" /><asp:Image ID="ImageButton4" runat="server" ImageUrl="~/Resources/Theme1.png" />
                </td>
                <td style="width: 80px; text-align: center;">
                    <asp:RadioButton ID="RadioButton2" GroupName="Theme" runat="server" OnCheckedChanged="RadioButton2_CheckedChanged"
                        AutoPostBack="true" /><asp:Image ID="ImageButton5" runat="server" ImageUrl="~/Resources/Theme2.png" />
                </td>
                <td style="width: 80px; text-align: center;">
                    <asp:RadioButton ID="RadioButton3" GroupName="Theme" runat="server" OnCheckedChanged="RadioButton3_CheckedChanged"
                        AutoPostBack="true" /><asp:Image ID="ImageButton6" runat="server" ImageUrl="~/Resources/Theme3.png" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="600px" Width="100%">
    </ActiveReportsWeb:WebViewer>
</asp:Content>
