﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="SupermarktBill.aspx.cs" Inherits="ActiveReportsDemo.Viewers.SupermarktBill" %>
<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
            <asp:Panel ID="Panel1" runat="server">
            <asp:Label ID="Label1" runat="server" Text="请输入商品数量"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="加载报表" onclick="Button1_Click" OnClientClick="return isInteger()" />
        </asp:Panel>
        <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="650px" Width="100%">
        </ActiveReportsWeb:WebViewer>
    <script type="text/javascript" language="javascript">
        function isInteger() {
            var numStr = document.getElementById('MainContainer_ReportContainer_TextBox1').value;
            var reg = new RegExp("^[0-9]*$");
            var result = reg.test(numStr);
            if (!result)
            {
                alert("请输入数字");
                return false;
            }
            if (numStr > 500)
            {
                alert("数字小于500");
                return false
            }
                
            return  true;
        }
    </script>
</asp:Content>
