﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrapeCity.ActiveReports.Web.Controls;

namespace ActiveReportsDemo.Viewers
{
    public partial class FormatPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomizeToolbarToolStrip();

                System.Xml.Linq.XElement xEle;

                if (Cache["ControlList"] == null)
                {
                    xEle = System.Xml.Linq.XElement.Load(Server.MapPath("~/ControlList.xml"));
                    Cache["ControlList"] = xEle;
                }
                else
                {
                    xEle = (System.Xml.Linq.XElement)Cache["ControlList"];
                }

                string categoryid = Request.QueryString["category"];
                string actionid = Request.QueryString["action"];
                string report = xEle.Elements("Control").Where(c => c.Attribute("id").Value == categoryid).First().Elements("action").Where(a => a.Attribute("id").Value == actionid).First().Attribute("report").Value;
                string viewtype = xEle.Elements("Control").Where(c => c.Attribute("id").Value == categoryid).First().Elements("action").Where(a => a.Attribute("id").Value == actionid).First().Attribute("viewtype").Value;

                switch (viewtype.ToLower())
                {
                    case "htmlviewer":
                        this.WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.HtmlViewer;
                        break;
                    case "rawhtml":
                        this.WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.RawHtml;
                        break;
                    case "acrobatreader":
                        this.WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.AcrobatReader;
                        break;
                    case "flashviewer":
                        this.WebViewer1.FlashViewerOptions.ResourceLocale = "zh_CN";
                        this.WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.FlashViewer;
                        break;
                    default:
                        break;
                }

                LoadReport(report);

            }
        }

        protected override void OnInit(EventArgs e)
        {
            System.Xml.Linq.XElement xEle;

            if (Cache["ControlList"] == null)
            {
                xEle = System.Xml.Linq.XElement.Load(Server.MapPath("~/ControlList.xml"));
                Cache["ControlList"] = xEle;
            }
            else
            {
                xEle = (System.Xml.Linq.XElement)Cache["ControlList"];
            }

            string categoryid = Request.QueryString["category"];
            string actionid = Request.QueryString["action"];

            string height = xEle.Elements("Control").Where(c => c.Attribute("id").Value == categoryid).First().Elements("action").Where(a => a.Attribute("id").Value == actionid).First().Attribute("height").Value;

            this.WebViewer1.Height = Unit.Pixel(Convert.ToInt32(height));

            base.OnInit(e);
        }

        private void LoadReport(string report)
        {
            GrapeCity.ActiveReports.PageReport rptPreview = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("../Reports/" + report + ".rdlx")));
            rptPreview.Report.DataSources[0].DataSourceReference = "";
            rptPreview.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            rptPreview.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("../Data/NWind_CHS.mdb"));
            rptPreview.Report.ReportParameters[0].DefaultValue.Values.Clear();
            rptPreview.Report.ReportParameters[0].DefaultValue.Values.Add("False");
            WebViewer1.Report = rptPreview;

            GrapeCity.ActiveReports.PageReport rptPrint = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("../Reports/" + report + ".rdlx")));
            rptPrint.Report.DataSources[0].DataSourceReference = "";
            rptPrint.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            rptPrint.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("../Data/NWind_CHS.mdb"));
            rptPrint.Report.ReportParameters[0].DefaultValue.Values.Clear();
            rptPrint.Report.ReportParameters[0].DefaultValue.Values.Add("True");
            WebViewer2.Report = rptPrint;
        }

        private void CustomizeToolbarToolStrip()
        {
            // 隐藏打印范围按钮
            ToolBase btnRang = WebViewer1.FlashViewerToolBar.Tools["PageRangeButton"];
            btnRang.Visible = false;

            // 隐藏打印按钮
            ToolBase btnPrint = WebViewer1.FlashViewerToolBar.Tools["PrintButton"];
            btnRang.Visible = false;

            ToolButton btnCPrint = Tool.CreateButton("套打");
            btnCPrint.Caption = "套打";
            btnCPrint.ToolTip = "套打";

            WebViewer1.FlashViewerToolBar.Tools.Insert(0, btnCPrint);

        }
    }
}