﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActiveReportsDemo.Viewers
{
    public partial class Export : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadPageReport("rptInvoice");
        }

        private void LoadPageReport(string report)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;
            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            report1.Report.DataSources[0].DataSourceReference = "";
            report1.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.FlashViewer;
            WebViewer1.Report = report1;
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string report = "rptInvoice";

            // Provide the page report you want to render.
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            // Create an output directory
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            // Provide settings for your rendering output.
            GrapeCity.ActiveReports.Export.Excel.Page.ExcelRenderingExtensionSettings
            excelSetting = new GrapeCity.ActiveReports.Export.Excel.Page.ExcelRenderingExtensionSettings();
            excelSetting.FileFormat = GrapeCity.ActiveReports.Export.Excel.Page.FileFormat.Xls;
            GrapeCity.ActiveReports.Extensibility.Rendering.ISettings setting = excelSetting;

            //Set the rendering extension and render the report.
            GrapeCity.ActiveReports.Export.Excel.Page.ExcelRenderingExtension
            excelRenderingExtension = new
            GrapeCity.ActiveReports.Export.Excel.Page.ExcelRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider outputProvider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            _reportRuntime.Render(excelRenderingExtension, outputProvider, excelSetting.GetSettings());

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "inline;filename=客户订单.xls");
            outputProvider.GetPrimaryStream().OpenStream().CopyTo(ms);
            Response.BinaryWrite(ms.ToArray());
            Response.End();
            /**
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("../") + "Reports/" + report + ".rdlx"));
            _reportDef.Report.DataSources[0].DataSourceReference = Server.MapPath("../Data/NWind_CHS_Access.rdsx");
            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);
            GrapeCity.ActiveReports.Export.Excel.Section.XlsExport XlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            XlsExport1.FileFormat = GrapeCity.ActiveReports.Export.Excel.Section.FileFormat.Xlsx;
            XlsExport1.Export(_reportRuntime, ms);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", Server.UrlPathEncode("attachment;filename=客户订单.xlsx"));
            Response.BinaryWrite(ms.ToArray());
            Response.End();
            */
        }

        protected void btnWord_Click(object sender, EventArgs e)
        {
            // Provide the page report you want to render.
            string report = "rptInvoice";
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            // Create an output directory
            System.IO.MemoryStream ms = new System.IO.MemoryStream();


            // Provide settings for your rendering output.
            GrapeCity.ActiveReports.Export.Word.Page.Settings wordSetting = new
            GrapeCity.ActiveReports.Export.Word.Page.Settings();
            wordSetting.UseMhtOutput = true;
            GrapeCity.ActiveReports.Extensibility.Rendering.ISettings setting = wordSetting;

            //Set the rendering extension and render the report.
            GrapeCity.ActiveReports.Export.Word.Page.WordRenderingExtension wordRenderingExtension =
            new GrapeCity.ActiveReports.Export.Word.Page.WordRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider outputProvider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            _reportRuntime.Render(wordRenderingExtension, outputProvider, wordSetting);

            Response.ContentType = "application/msword";
            Response.AddHeader("content-disposition", Server.UrlPathEncode("attachment;filename=客户订单.doc"));
            outputProvider.GetPrimaryStream().OpenStream().CopyTo(ms);
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void btnPdf_Click(object sender, EventArgs e)
        {
            // Provide the page report you want to render.
            string report = "rptInvoice";
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            // Create an output directory
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            // Provide settings for your rendering output.
            GrapeCity.ActiveReports.Export.Pdf.Page.Settings pdfSetting = new
            GrapeCity.ActiveReports.Export.Pdf.Page.Settings();
            GrapeCity.ActiveReports.Extensibility.Rendering.ISettings setting = pdfSetting;
            //Set the rendering extension and render the report.
            GrapeCity.ActiveReports.Export.Pdf.Page.PdfRenderingExtension pdfRenderingExtension =
            new GrapeCity.ActiveReports.Export.Pdf.Page.PdfRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider outputProvider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            _reportRuntime.Render(pdfRenderingExtension, outputProvider, pdfSetting);

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "inline;filename=客户订单.pdf");
            outputProvider.GetPrimaryStream().OpenStream().CopyTo(ms);
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void btnHtml_Click(object sender, EventArgs e)
        {
            // Provide the page report you want to render.
            string report = "rptInvoice";
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            // Create an output directory
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            // Provide settings for your rendering output.
            GrapeCity.ActiveReports.Export.Html.Page.Settings htmlSetting = new
            GrapeCity.ActiveReports.Export.Html.Page.Settings();
            htmlSetting.StyleStream = false;
            htmlSetting.MhtOutput = false;
            htmlSetting.Fragment = false;
            htmlSetting.OutputTOC = true;
            htmlSetting.Mode = GrapeCity.ActiveReports.Export.Html.Page.RenderMode.Paginated;

            GrapeCity.ActiveReports.Extensibility.Rendering.ISettings setting = htmlSetting;

            //Set the rendering extension and render the report.
            GrapeCity.ActiveReports.Export.Html.Page.HtmlRenderingExtension htmlRenderingExtension = new GrapeCity.ActiveReports.Export.Html.Page.HtmlRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider outputProvider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            _reportRuntime.Render(htmlRenderingExtension, outputProvider, htmlSetting);

            Response.ContentType = "text/html";
            Response.AddHeader("content-disposition", "inline;filename=客户订单.html");
            outputProvider.GetPrimaryStream().OpenStream().CopyTo(ms);
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void btnText_Click(object sender, EventArgs e)
        {
            // Provide the page report you want to render.
            string report = "rptInvoice";
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/") + "Reports/" + report + ".rdlx"));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            GrapeCity.ActiveReports.Export.Xml.Section.TextExport txtExport1 = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();
            txtExport1.Encoding = Encoding.Unicode;

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            txtExport1.Export(_reportRuntime, ms);

            Response.ContentType = "text/plain";
            Response.AddHeader("content-disposition", Server.UrlPathEncode("attachment;filename=客户订单.txt"));
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void btnCSV_Click(object sender, EventArgs e)
        {
            // Provide the page report you want to render.
            string report = "rptInvoice";
            GrapeCity.ActiveReports.PageReport _reportDef = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/") + "Reports/" + report + ".rdlx"));
            _reportDef.Report.DataSources[0].DataSourceReference = "";
            _reportDef.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            _reportDef.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument _reportRuntime = new GrapeCity.ActiveReports.Document.PageDocument(_reportDef);

            GrapeCity.ActiveReports.Export.Xml.Section.TextExport csvExport1 = new GrapeCity.ActiveReports.Export.Xml.Section.TextExport();
            csvExport1.Encoding = Encoding.Unicode;
            csvExport1.TextDelimiter = "\t";
            csvExport1.SuppressEmptyLines = true;

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            csvExport1.Export(_reportRuntime, ms);

            Response.ContentType = "text/plain";
            Response.AddHeader("content-disposition", Server.UrlPathEncode("attachment;filename=客户订单.csv"));
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void btnImage_Click(object sender, EventArgs e)
        {
            string reportname = "rptInvoice";
            GrapeCity.ActiveReports.PageReport report = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + reportname + ".rdlx")));
            report.Report.DataSources[0].DataSourceReference = "";
            report.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            report.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            GrapeCity.ActiveReports.Document.PageDocument document = new GrapeCity.ActiveReports.Document.PageDocument(report);

            GrapeCity.ActiveReports.Export.Image.Page.Settings settings = new GrapeCity.ActiveReports.Export.Image.Page.Settings();
            settings.ImageType = GrapeCity.ActiveReports.Export.Image.Page.Renderers.ImageType.JPEG;


            settings.StartPage = 1;
            settings.EndPage = 1;

            GrapeCity.ActiveReports.Export.Image.Page.ImageRenderingExtension renderingExtension = new GrapeCity.ActiveReports.Export.Image.Page.ImageRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider _provider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            document.Render(renderingExtension, _provider, settings);

            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            _provider.GetSecondaryStreams()[0].OpenStream().CopyTo(ms);

            Response.ContentType = "image/jpg";
            Response.AddHeader("content-disposition", Server.UrlPathEncode("attachment;filename=客户订单.jpg"));
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }


    }
}