﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActiveReportsDemo.Viewers
{
    public partial class TocPanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadPageReport("rptMonthlySalesByCategory");
        }

        private void LoadPageReport(string report)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;
            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            report1.Report.DataSources[0].DataSourceReference = "";
            report1.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            WebViewer1.Report = report1;
        }
    }
}