﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActiveReportsDemo.Viewers
{
    public partial class Theme : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RadioButton1.Checked = true;
            LoadPageReport("rptTheme1", 1);
        }

        private void LoadPageReport(string report, int themeid)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;
            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            report1.Report.DataSources[0].DataSourceReference = "";
            report1.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            report1.Report.Themes.Clear();
            report1.Report.Themes.Add(Server.MapPath(string.Format("~/Theme/Style{0}.rdlx-theme", themeid)));
            WebViewer1.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.FlashViewer;
            WebViewer1.Report = report1;
        }


        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            LoadPageReport("rptTheme1", 1);
        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            LoadPageReport("rptTheme1", 2);
        }

        protected void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            LoadPageReport("rptTheme1", 3);
        }
    }
}