﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ActiveReportsDemo.Viewers
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //获取当前请求的地址路径及参数
            //取URL传参
            string categoryID = !String.IsNullOrEmpty(Request.QueryString["category"]) ? Request.QueryString["category"].ToString() : "";
            string actionID = !String.IsNullOrEmpty(Request.QueryString["action"]) ? Request.QueryString["action"].ToString() : "";
            //判断是否为报表库
            string reportType = !String.IsNullOrEmpty(Request.QueryString["t"]) ? Request.QueryString["t"].ToString() : "";

            string reportID;
            if (String.IsNullOrEmpty(reportType))
            {
                //获得xml文件中的ReportID
                reportID = getReportIDFromSiteMenu(actionID,categoryID,"Feature");
            }
            else
            {
                reportID = getReportIDFromSiteMenu(actionID, categoryID, "Library");
            }

            if (string.IsNullOrEmpty(reportID))
            {
                return;
            }
            else
            {
                XElement xmlDoc = loadXml("ReportResources");

                ResourceList rl = new ResourceList(reportID, xmlDoc);

                if (String.IsNullOrEmpty(reportType))
                {
                    rl.LoadReport(Server, WebViewer1, "NWind_CHS");
                }
                else
                {
                    rl.LoadReport(Server, WebViewer1, "BaseInformation");
                }
            }
        }

        private void LoadPageReport(string report)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;

            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + report + ".rdlx")));
            report1.Report.DataSources[0].DataSourceReference = "";
            report1.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";

            report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/NWind_CHS.mdb"));

            WebViewer1.Report = report1;
        }

        private string getReportIDFromSiteMenu(string actionid, string categoryid, String xmlName)
        {
            XElement doc = loadXml(xmlName);

            string ReportID = "";
            if (String.IsNullOrEmpty(categoryid))
            {
                return "";
            }

            var rootmenu = doc.Elements("First").Where(c => c.Attribute("Categoryid").Value == categoryid).First();
            var node = rootmenu.Elements("Second").Where(c => c.Attribute("Action").Value == actionid).FirstOrDefault();

            if (node == null)
            {
                foreach (XElement item in rootmenu.Elements("Second"))
                {
                    node = item.Elements("Third").Where(c => c.Attribute("Action").Value == actionid).FirstOrDefault();
                    if (node != null)
                    {
                        break;
                    }
                }
            }

            ReportID = node.Attribute("ReportID").Value;
            return ReportID;
        }

        private System.Xml.Linq.XElement loadXml(String xmlName)
        {
            //获得XML文件服务器地址  siteMenu.xml
            string siteMenuPath = Server.MapPath("~/config/" + xmlName + ".xml");

            //防止异常，判断XML是否存在
            if (!File.Exists(siteMenuPath))
            {
                Response.Write("<script>alert('配置文件错误，请稍后重试！')</script>");
                return null;
            }

            XElement xmlDoc = null;

            String cachaName = "c_" + xmlName;
            //判断缓存，若为空则载入XML文件，相反则直接从内存中调用菜单信息
            if (HttpContext.Current.Cache[cachaName] == null)
            {
                xmlDoc = XElement.Load(siteMenuPath); 
                //写入缓存
                HttpContext.Current.Cache.Insert(cachaName, xmlDoc, new CacheDependency(siteMenuPath, DateTime.Now));
            }
            else
            {
                xmlDoc = (System.Xml.Linq.XElement)HttpContext.Current.Cache[cachaName];
            }

            return xmlDoc;
        }
    }
}