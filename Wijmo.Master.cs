﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;

namespace ActiveReportsDemo
{
    public partial class Wijmo : System.Web.UI.MasterPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
            {
                //themeswitcher.Visible = false;
                C1Accordion1.Theme = "metro";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            BindNavigation();
            RegisterCallbacks();
        }

        private void BindNavigation()
        {
            System.Xml.Linq.XElement xEle;

            string pagename = Request.Url.PathAndQuery;
            string categoryid = Request.QueryString["category"];
            string actionid = Request.QueryString["action"];

            C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane selectedpane = null;

            if (Cache["ControlList"] == null)
            {
                xEle = System.Xml.Linq.XElement.Load(Server.MapPath("~/ControlList.xml"));
                Cache["ControlList"] = xEle;
            }
            else
            {
                xEle = (System.Xml.Linq.XElement)Cache["ControlList"];
            }

            IEnumerable<XElement> catalogs = from c in xEle.Elements("Control") select c;

            foreach (XElement category in catalogs)
            {
                // 添加报表分类
                C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane pane = new C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane();
                Label header = new Label();
                header.Text = category.Attribute("text").Value;
                header.Attributes.Add("style", "font-size:16px;");                
                pane.HeaderPanel.Controls.Add(header);
                
                HtmlGenericControl ulSideMenu = new HtmlGenericControl("ul");
                ulSideMenu.Attributes["class"] = "sample-list";
                
                pane.ContentPanel.Controls.Add(ulSideMenu);
                pane.ContentPanel.BorderStyle = BorderStyle.None;

                if (categoryid == category.Attribute("id").Value)
                {
                    selectedpane = pane;
                    pane.HeaderPanel.Attributes.Add("style", "background:#3A4554;color:#ffffff;");
                }
                else
                {
                    pane.HeaderPanel.Attributes.Add("style", "background:#cccccc;color:#ffffff;");
                }

                foreach (XElement action in category.Elements("action"))
                {
                    // 添加分类下的功能
                    HtmlGenericControl li = new HtmlGenericControl("li");

                    ulSideMenu.Controls.Add(li);
                    HtmlGenericControl link = new HtmlGenericControl("a");
                    link.InnerHtml = action.Attribute("text").Value;
                    link.Attributes["href"] = ResolveClientUrl(string.Format("{0}?category={1}&action={2}", action.Attribute("page").Value, category.Attribute("id").Value, action.Attribute("id").Value));
                    link.Attributes.Add("style", "font-size:16px");
                    li.Controls.Add(link);

                    li.Attributes["class"] = "demo-config-on";                    

                    if (category.Attribute("id").Value == categoryid && action.Attribute("id").Value == actionid)
                    {
                        link.Attributes.Add("style", "font-size:16px;color:teal;");
                        WidgetNameLebel.Text = string.Format("{0}——{1}", action.Parent.Attribute("text").Value, action.Attribute("text").Value);
                        WidgetDescriptioneLebel.Text = action.Element("Description").Value;
                    }
                }

                C1Accordion1.Panes.Add(pane);
            }

            C1Accordion1.SelectedPane = selectedpane;

        }
    }
}