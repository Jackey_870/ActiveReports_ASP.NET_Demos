namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptMonthlySalesByCategory.
    /// </summary>
    partial class rptMonthlySalesByCategory
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMonthlySalesByCategory));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txt数量 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt单价 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt折扣 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt产品名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.crossSectionBox1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.txt订购月1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt数量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt销售额1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txt类别1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.crossSectionBox2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txt数量2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt销售额2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt类别2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt数量3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt销售额3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt折扣)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购月1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt数量,
            this.txt单价,
            this.txt折扣});
            this.detail.Height = 0.25F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txt数量
            // 
            this.txt数量.DataField = "数量";
            this.txt数量.Height = 0.25F;
            this.txt数量.Left = 3.98F;
            this.txt数量.Name = "txt数量";
            this.txt数量.Text = "txt数量";
            this.txt数量.Top = 0F;
            this.txt数量.Width = 0.7289996F;
            // 
            // txt单价
            // 
            this.txt单价.DataField = "单价";
            this.txt单价.Height = 0.25F;
            this.txt单价.Left = 4.709F;
            this.txt单价.Name = "txt单价";
            this.txt单价.Text = "txt单价";
            this.txt单价.Top = 0F;
            this.txt单价.Width = 0.7289993F;
            // 
            // txt折扣
            // 
            this.txt折扣.DataField = "折扣";
            this.txt折扣.Height = 0.25F;
            this.txt折扣.Left = 5.438F;
            this.txt折扣.Name = "txt折扣";
            this.txt折扣.Text = "txt折扣";
            this.txt折扣.Top = 0F;
            this.txt折扣.Width = 0.7289993F;
            // 
            // txt产品名称1
            // 
            this.txt产品名称1.DataField = "产品名称";
            this.txt产品名称1.Height = 0.25F;
            this.txt产品名称1.Left = 1.98F;
            this.txt产品名称1.Name = "txt产品名称1";
            this.txt产品名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(3, 0, 0, 0);
            this.txt产品名称1.Style = "text-align: left";
            this.txt产品名称1.Text = "txt产品名称1";
            this.txt产品名称1.Top = 0F;
            this.txt产品名称1.Width = 2F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1});
            this.pageHeader.Height = 0.6F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.DataField = "= 订购年 + \"年各月产品销售分类汇总\"";
            this.label1.Height = 0.5F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center";
            this.label1.Text = "";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.3F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.3F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.crossSectionBox1,
            this.txt订购月1,
            this.textBox2});
            this.groupHeader1.DataField = "订购月";
            this.groupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.groupHeader1.Height = 0.3F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.groupHeader1.Format += new System.EventHandler(this.groupHeader1_Format);
            // 
            // crossSectionBox1
            // 
            this.crossSectionBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.crossSectionBox1.Bottom = 4.470348E-08F;
            this.crossSectionBox1.Left = 0F;
            this.crossSectionBox1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.crossSectionBox1.LineWeight = 0F;
            this.crossSectionBox1.Name = "crossSectionBox1";
            this.crossSectionBox1.Right = 1F;
            this.crossSectionBox1.Top = 0F;
            // 
            // txt订购月1
            // 
            this.txt订购月1.DataField = "=订购年 + \"年\" + 订购月 + \"月\"";
            this.txt订购月1.Height = 0.3F;
            this.txt订购月1.Left = 0F;
            this.txt订购月1.Name = "txt订购月1";
            this.txt订购月1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt订购月1.Style = "background-color: #C04000; color: White; font-size: 14pt; font-weight: bold; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 1";
            this.txt订购月1.Text = "txt订购月1";
            this.txt订购月1.Top = 0F;
            this.txt订购月1.Width = 1.27F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "=\" 各类商品销售明细\"";
            this.textBox2.Height = 0.3F;
            this.textBox2.Left = 1.27F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.textBox2.Style = "background-color: #C04000; color: White; font-size: 14pt; font-weight: bold; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox2.Text = "txt订购月1";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 6F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.txt数量1,
            this.txt销售额1});
            this.groupFooter1.Height = 0.5F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.25F;
            this.textBox1.Left = 1.98F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Style = "color: #C04000; font-size: 12pt; font-weight: bold; text-align: right; ddo-char-s" +
    "et: 1";
            this.textBox1.Text = "当月合计：";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 2F;
            // 
            // txt数量1
            // 
            this.txt数量1.DataField = "数量";
            this.txt数量1.Height = 0.25F;
            this.txt数量1.Left = 3.98F;
            this.txt数量1.Name = "txt数量1";
            this.txt数量1.Style = "color: #C04000; font-size: 12pt; font-weight: bold; text-align: center; ddo-char-" +
    "set: 1";
            this.txt数量1.SummaryGroup = "groupHeader1";
            this.txt数量1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txt数量1.Text = "txt数量1";
            this.txt数量1.Top = 0F;
            this.txt数量1.Width = 1F;
            // 
            // txt销售额1
            // 
            this.txt销售额1.Height = 0.25F;
            this.txt销售额1.Left = 4.980001F;
            this.txt销售额1.Name = "txt销售额1";
            this.txt销售额1.OutputFormat = resources.GetString("txt销售额1.OutputFormat");
            this.txt销售额1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt销售额1.Style = "color: #C04000; font-size: 12pt; font-weight: bold; text-align: right; ddo-char-s" +
    "et: 1";
            this.txt销售额1.Text = "txt销售额1";
            this.txt销售额1.Top = 0F;
            this.txt销售额1.Width = 2.29F;
            // 
            // groupHeader2
            // 
            this.groupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt类别1,
            this.crossSectionBox2,
            this.label2,
            this.label3,
            this.label4,
            this.textBox3});
            this.groupHeader2.DataField = "类别名称";
            this.groupHeader2.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.groupHeader2.Height = 0.5104166F;
            this.groupHeader2.Name = "groupHeader2";
            this.groupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.groupHeader2.Format += new System.EventHandler(this.groupHeader2_Format);
            // 
            // txt类别1
            // 
            this.txt类别1.DataField = "=类别名称";
            this.txt类别1.Height = 0.25F;
            this.txt类别1.Left = 1F;
            this.txt类别1.Name = "txt类别1";
            this.txt类别1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt类别1.Style = "background-color: #FFC080; color: White; font-size: 12pt; font-weight: bold; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 1";
            this.txt类别1.Text = "txt类别1";
            this.txt类别1.Top = 0F;
            this.txt类别1.Width = 1.27F;
            // 
            // crossSectionBox2
            // 
            this.crossSectionBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.crossSectionBox2.Bottom = 0.3958333F;
            this.crossSectionBox2.Left = 1F;
            this.crossSectionBox2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.crossSectionBox2.LineWeight = 0F;
            this.crossSectionBox2.Name = "crossSectionBox2";
            this.crossSectionBox2.Right = 1.98F;
            this.crossSectionBox2.Top = 0F;
            // 
            // label2
            // 
            this.label2.Height = 0.25F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.98F;
            this.label2.Name = "label2";
            this.label2.Padding = new GrapeCity.ActiveReports.PaddingEx(3, 0, 0, 0);
            this.label2.Style = "background-color: #FFC0AF; color: White; font-weight: bold; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.label2.Text = "产品名称";
            this.label2.Top = 0.25F;
            this.label2.Width = 2F;
            // 
            // label3
            // 
            this.label3.Height = 0.25F;
            this.label3.HyperLink = null;
            this.label3.Left = 3.98F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #FFC0AF; color: White; font-weight: bold; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.label3.Text = "销售量";
            this.label3.Top = 0.25F;
            this.label3.Width = 1F;
            // 
            // label4
            // 
            this.label4.Height = 0.25F;
            this.label4.HyperLink = null;
            this.label4.Left = 4.98F;
            this.label4.Name = "label4";
            this.label4.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.label4.Style = "background-color: #FFC0AF; color: White; font-weight: bold; text-align: right; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.label4.Text = "销售金额";
            this.label4.Top = 0.25F;
            this.label4.Width = 2.290001F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "=\"  (包括：\" + 说明 + \")\"";
            this.textBox3.Height = 0.25F;
            this.textBox3.Left = 2.27F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.textBox3.Style = "background-color: #FFC080; color: White; font-size: 12pt; font-weight: bold; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 1";
            this.textBox3.Text = "txt类别1";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 5F;
            // 
            // groupFooter2
            // 
            this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt数量2,
            this.txt销售额2,
            this.txt类别2});
            this.groupFooter2.Height = 0.36875F;
            this.groupFooter2.Name = "groupFooter2";
            this.groupFooter2.Format += new System.EventHandler(this.groupFooter2_Format);
            // 
            // txt数量2
            // 
            this.txt数量2.DataField = "数量";
            this.txt数量2.Height = 0.25F;
            this.txt数量2.Left = 3.98F;
            this.txt数量2.Name = "txt数量2";
            this.txt数量2.Style = "font-weight: bold; text-align: center; ddo-char-set: 1";
            this.txt数量2.SummaryGroup = "groupHeader2";
            this.txt数量2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txt数量2.Text = "txt数量2";
            this.txt数量2.Top = 0F;
            this.txt数量2.Width = 1F;
            // 
            // txt销售额2
            // 
            this.txt销售额2.Height = 0.25F;
            this.txt销售额2.Left = 4.980001F;
            this.txt销售额2.Name = "txt销售额2";
            this.txt销售额2.OutputFormat = resources.GetString("txt销售额2.OutputFormat");
            this.txt销售额2.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt销售额2.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txt销售额2.Text = "txt销售额2";
            this.txt销售额2.Top = 0F;
            this.txt销售额2.Width = 2.29F;
            // 
            // txt类别2
            // 
            this.txt类别2.Height = 0.25F;
            this.txt类别2.Left = 1.98F;
            this.txt类别2.Name = "txt类别2";
            this.txt类别2.OutputFormat = resources.GetString("txt类别2.OutputFormat");
            this.txt类别2.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.txt类别2.Text = "分类小计：";
            this.txt类别2.Top = 0F;
            this.txt类别2.Width = 2F;
            // 
            // txt数量3
            // 
            this.txt数量3.DataField = "数量";
            this.txt数量3.Height = 0.25F;
            this.txt数量3.Left = 3.98F;
            this.txt数量3.Name = "txt数量3";
            this.txt数量3.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt数量3.Style = "text-align: center";
            this.txt数量3.SummaryGroup = "groupHeader3";
            this.txt数量3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txt数量3.Text = "txt数量3";
            this.txt数量3.Top = 0F;
            this.txt数量3.Width = 1F;
            // 
            // txt销售额3
            // 
            this.txt销售额3.Height = 0.25F;
            this.txt销售额3.Left = 4.98F;
            this.txt销售额3.Name = "txt销售额3";
            this.txt销售额3.OutputFormat = resources.GetString("txt销售额3.OutputFormat");
            this.txt销售额3.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 3, 0);
            this.txt销售额3.Style = "text-align: right";
            this.txt销售额3.Text = "txt销售额3";
            this.txt销售额3.Top = 0F;
            this.txt销售额3.Width = 2.29F;
            // 
            // groupHeader3
            // 
            this.groupHeader3.DataField = "产品名称";
            this.groupHeader3.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.groupHeader3.Height = 0F;
            this.groupHeader3.Name = "groupHeader3";
            this.groupHeader3.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // groupFooter3
            // 
            this.groupFooter3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt数量3,
            this.txt销售额3,
            this.txt产品名称1});
            this.groupFooter3.Name = "groupFooter3";
            this.groupFooter3.Format += new System.EventHandler(this.groupFooter3_Format);
            // 
            // rptMonthlySalesByCategory
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = resources.GetString("oleDBDataSource1.SQL");
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.270001F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.groupHeader2);
            this.Sections.Add(this.groupHeader3);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter3);
            this.Sections.Add(this.groupFooter2);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: RosyBrown", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #FFC0C0", "行间隔色", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptMonthlySalesByCategory_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txt数量)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt折扣)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购月1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt销售额3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt订购月1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt销售额3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt产品名称1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt类别1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader3;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt销售额2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt类别2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt销售额1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt单价;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt折扣;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
    }
}
