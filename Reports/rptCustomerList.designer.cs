namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptCustomerList.
    /// </summary>
    partial class rptCustomerList
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomerList));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txt客户ID1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt公司名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt联系人姓名1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt联系人职务1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt电话1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt地址1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.crossSectionBox1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            ((System.ComponentModel.ISupportInitialize)(this.txt客户ID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt公司名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人职务1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt电话1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt地址1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt客户ID1,
            this.txt公司名称1,
            this.txt联系人姓名1,
            this.txt联系人职务1,
            this.txt电话1,
            this.txt地址1});
            this.detail.Height = 0.25F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txt客户ID1
            // 
            this.txt客户ID1.DataField = "客户ID";
            this.txt客户ID1.Height = 0.25F;
            this.txt客户ID1.Left = 0F;
            this.txt客户ID1.Name = "txt客户ID1";
            this.txt客户ID1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt客户ID1.Style = "text-align: left";
            this.txt客户ID1.Text = "txt客户ID1";
            this.txt客户ID1.Top = 0F;
            this.txt客户ID1.Width = 1F;
            // 
            // txt公司名称1
            // 
            this.txt公司名称1.DataField = "公司名称";
            this.txt公司名称1.Height = 0.25F;
            this.txt公司名称1.Left = 1F;
            this.txt公司名称1.Name = "txt公司名称1";
            this.txt公司名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt公司名称1.ShrinkToFit = true;
            this.txt公司名称1.Style = "text-align: left; ddo-shrink-to-fit: true";
            this.txt公司名称1.Text = "txt公司名称1";
            this.txt公司名称1.Top = 0F;
            this.txt公司名称1.Width = 1.4F;
            // 
            // txt联系人姓名1
            // 
            this.txt联系人姓名1.DataField = "联系人姓名";
            this.txt联系人姓名1.Height = 0.25F;
            this.txt联系人姓名1.Left = 2.4F;
            this.txt联系人姓名1.Name = "txt联系人姓名1";
            this.txt联系人姓名1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt联系人姓名1.Style = "text-align: left";
            this.txt联系人姓名1.Text = "txt联系人姓名1";
            this.txt联系人姓名1.Top = 0F;
            this.txt联系人姓名1.Width = 0.7F;
            // 
            // txt联系人职务1
            // 
            this.txt联系人职务1.DataField = "联系人职务";
            this.txt联系人职务1.Height = 0.25F;
            this.txt联系人职务1.Left = 3.1F;
            this.txt联系人职务1.Name = "txt联系人职务1";
            this.txt联系人职务1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt联系人职务1.Style = "text-align: left";
            this.txt联系人职务1.Text = "txt联系人职务1";
            this.txt联系人职务1.Top = 0F;
            this.txt联系人职务1.Width = 1.2F;
            // 
            // txt电话1
            // 
            this.txt电话1.DataField = "电话";
            this.txt电话1.Height = 0.25F;
            this.txt电话1.Left = 4.3F;
            this.txt电话1.Name = "txt电话1";
            this.txt电话1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt电话1.Style = "text-align: left";
            this.txt电话1.Text = "txt电话1";
            this.txt电话1.Top = 0F;
            this.txt电话1.Width = 1.4F;
            // 
            // txt地址1
            // 
            this.txt地址1.DataField = "=城市 + \"市\" + 地址";
            this.txt地址1.Height = 0.25F;
            this.txt地址1.Left = 5.7F;
            this.txt地址1.Name = "txt地址1";
            this.txt地址1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt地址1.ShrinkToFit = true;
            this.txt地址1.Style = "text-align: left; ddo-shrink-to-fit: true";
            this.txt地址1.Text = "txt地址1";
            this.txt地址1.Top = 0F;
            this.txt地址1.Width = 1.57F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label9,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.textBox1,
            this.crossSectionBox1});
            this.pageHeader.Height = 1.283F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label9
            // 
            this.label9.ClassName = "列头-10-加粗-背景";
            this.label9.Height = 0.2500001F;
            this.label9.HyperLink = null;
            this.label9.Left = 0F;
            this.label9.Name = "label9";
            this.label9.Style = "";
            this.label9.Text = "";
            this.label9.Top = 1.033F;
            this.label9.Width = 7.27F;
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.783F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; vertical-align: middle";
            this.label1.Text = "客户信息清单";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // label2
            // 
            this.label2.ClassName = "列头-10-加粗-背景";
            this.label2.Height = 0.25F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: #39A3C4; color: White";
            this.label2.Text = "客户编号";
            this.label2.Top = 1.033F;
            this.label2.Width = 1F;
            // 
            // label3
            // 
            this.label3.ClassName = "列头-10-加粗-背景";
            this.label3.Height = 0.25F;
            this.label3.HyperLink = null;
            this.label3.Left = 1F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #39A3C4; color: White";
            this.label3.Text = "公司名称";
            this.label3.Top = 1.033F;
            this.label3.Width = 1.4F;
            // 
            // label4
            // 
            this.label4.ClassName = "列头-10-加粗-背景";
            this.label4.Height = 0.25F;
            this.label4.HyperLink = null;
            this.label4.Left = 2.4F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #39A3C4; color: White";
            this.label4.Text = "联系人";
            this.label4.Top = 1.033F;
            this.label4.Width = 0.7F;
            // 
            // label5
            // 
            this.label5.ClassName = "列头-10-加粗-背景";
            this.label5.Height = 0.25F;
            this.label5.HyperLink = null;
            this.label5.Left = 4.3F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: #39A3C4; color: White";
            this.label5.Text = "电话";
            this.label5.Top = 1.033F;
            this.label5.Width = 1.4F;
            // 
            // label6
            // 
            this.label6.ClassName = "列头-10-加粗-背景";
            this.label6.Height = 0.25F;
            this.label6.HyperLink = null;
            this.label6.Left = 5.7F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: #39A3C4; color: White";
            this.label6.Text = "地址";
            this.label6.Top = 1.033F;
            this.label6.Width = 1.57F;
            // 
            // label7
            // 
            this.label7.ClassName = "列头-10-加粗-背景";
            this.label7.Height = 0.25F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.1F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: #39A3C4; color: White";
            this.label7.Text = "联系人职务";
            this.label7.Top = 1.033F;
            this.label7.Width = 1.2F;
            // 
            // label8
            // 
            this.label8.Height = 0.25F;
            this.label8.HyperLink = null;
            this.label8.Left = 5.7F;
            this.label8.Name = "label8";
            this.label8.Style = "text-align: right";
            this.label8.Text = "客户总数：";
            this.label8.Top = 0.783F;
            this.label8.Width = 1.083F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "客户ID";
            this.textBox1.Height = 0.2499999F;
            this.textBox1.Left = 6.783F;
            this.textBox1.Name = "textBox1";
            this.textBox1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.textBox1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0.783F;
            this.textBox1.Width = 0.487F;
            // 
            // crossSectionBox1
            // 
            this.crossSectionBox1.Bottom = 0F;
            this.crossSectionBox1.Left = 0F;
            this.crossSectionBox1.LineColor = System.Drawing.Color.DarkGray;
            this.crossSectionBox1.LineWeight = 1F;
            this.crossSectionBox1.Name = "crossSectionBox1";
            this.crossSectionBox1.Right = 7.27F;
            this.crossSectionBox1.Top = 1.033F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.4F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.4F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // rptCustomerList
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "Select * from 客户";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.27F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: #CD6839", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: Gainsboro", "行间隔色", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txt客户ID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt公司名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人职务1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt电话1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt地址1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt客户ID1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt公司名称1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt联系人姓名1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt联系人职务1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt电话1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt地址1;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox crossSectionBox1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
    }
}
