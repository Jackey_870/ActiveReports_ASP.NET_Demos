using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptControls1.
    /// </summary>
    public partial class rptControls1 : GrapeCity.ActiveReports.SectionReport
    {

        public rptControls1()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public string BasePath
        { get; set; }

        // 添加 C1Gauge 控件
        private void AddGauge1()
        {
            C1.Win.C1Gauge.C1Gauge c1Gauge1 = new C1.Win.C1Gauge.C1Gauge();            
            c1Gauge1.Load(string.Format(@"{0}\Data\C1RadialGauge1.xml", BasePath));
            c1Gauge1.BackColor = Color.White;
            c1Gauge1.Gauges[0].Value = 80;
            c1Gauge1.Gauges[1].Value = 88;
            c1Gauge1.Gauges[2].Value = 75;

            picture1.Image = c1Gauge1.GetImage(450,300);
        }

        // 添加 C1Chart 控件
        private void AddChart1()
        {
            C1.Win.C1Chart.C1Chart c1Chart = new C1.Win.C1Chart.C1Chart();
            c1Chart.LoadChartFromFile(string.Format(@"{0}Data\C1Chart1.chart2dxml", BasePath));

            picture2.Image = c1Chart.GetImage(new Size(800,370));
        }

        private void rptControls1_ReportStart(object sender, EventArgs e)
        {
            AddGauge1();
            AddChart1();

            richTextBox1.LoadFile(string.Format(@"{0}Data\支持第三方控件.rtf", BasePath));
        }
    }
}
