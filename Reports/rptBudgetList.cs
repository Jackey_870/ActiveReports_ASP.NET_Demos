using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptBudgetList.
    /// </summary>
    public partial class rptBudgetList : SectionReportBase
    {

        public rptBudgetList()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        // ����ͼ�εĳ�ʼλ�úͿ���

        float left = 0;
        float width = 0;

        private void detail_Format(object sender, EventArgs e)
        {
            double budget = 0;
            double final = 0;

            // ��ʽ��Ԥ����
            if (this.txt��Ŀ���1.Text == "")
            {
                this.txt�����1.Value = "";
                this.txt�����1.Value = "";
                this.shape2.Width = 0;
                return;
            }

            if (this.txtԤ��1.Value.ToString() != "0")
            {
                budget = double.Parse(this.txtԤ��1.Value.ToString());
            }
            else
            {
                this.txtԤ��1.Text = "(  ��  )";
            }

            // ��ʽ��������
            if (this.txt����1.Value.ToString() != "0")
            {
                final = double.Parse(this.txt����1.Value.ToString());
            }
            else
            {
                this.txt����1.Text = "(  ��  )";
            }

            // ��ʽ���������Ͳ������
            if ((budget - final) == 0)
            {
                this.txt�����1.Text = "(  ��  )";
                this.txt�����1.Text = "(  ��  )";
                this.shape2.Width = 0;
            }
            else
            {
                this.txt�����1.Value = budget - final;

                float percentage = Convert.ToSingle(((budget - final) / budget));

                this.txt�����1.Value = percentage * 100;

                this.shape2.Width = Convert.ToSingle((width * Math.Abs(percentage)));

                // ����ͼ����ʾλ��
                if (percentage >= 0)
                {
                    this.shape2.Left = left;
                    this.shape2.BackColor = Color.Green;
                }
                else
                {
                    this.shape2.Left = left + (width * percentage);
                    this.shape2.BackColor = Color.Red;
                }


            }
        }

        private void rptBudgetList_ReportStart(object sender, EventArgs e)
        {
            width = this.shape2.Width / 2;
            left = this.shape2.Left + width;
        }
    }
}
