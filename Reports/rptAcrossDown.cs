using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptAcrossDown.
    /// </summary>
    public partial class rptAcrossDown : SectionReportBase
    {

        public rptAcrossDown()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        int RowNo = 0;
        private void detail_Format(object sender, EventArgs e)
        {
            if ((RowNo % 4) > 1)
            {
                this.shape1.BackColor = Color.Gainsboro;
            }
            else
            {
                this.shape1.BackColor = Color.Transparent;
            }

            RowNo++;
        }
    }
}
