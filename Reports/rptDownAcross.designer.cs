namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptDownAcross.
    /// </summary>
    partial class rptDownAcross
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDownAcross));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txt项目编号1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt项目编号2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt数量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.checkBox1 = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.ColumnCount = 2;
            this.detail.ColumnSpacing = 0.27F;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.txt项目编号1,
            this.txt项目编号2,
            this.txt数量1,
            this.checkBox1});
            this.detail.Height = 0.25F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // shape1
            // 
            this.shape1.Height = 0.25F;
            this.shape1.Left = 0F;
            this.shape1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape1.LineWeight = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0F;
            this.shape1.Width = 3.5F;
            // 
            // txt项目编号1
            // 
            this.txt项目编号1.DataField = "行号";
            this.txt项目编号1.Height = 0.25F;
            this.txt项目编号1.Left = 0F;
            this.txt项目编号1.Name = "txt项目编号1";
            this.txt项目编号1.Style = "font-weight: bold; vertical-align: middle; ddo-char-set: 1";
            this.txt项目编号1.Text = "No";
            this.txt项目编号1.Top = 0F;
            this.txt项目编号1.Width = 0.2F;
            // 
            // txt项目编号2
            // 
            this.txt项目编号2.DataField = "项目名称";
            this.txt项目编号2.Height = 0.25F;
            this.txt项目编号2.Left = 0.2F;
            this.txt项目编号2.Name = "txt项目编号2";
            this.txt项目编号2.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.txt项目编号2.Style = "text-align: left; vertical-align: middle";
            this.txt项目编号2.Text = "txt项目编号2";
            this.txt项目编号2.Top = 0F;
            this.txt项目编号2.Width = 2F;
            // 
            // txt数量1
            // 
            this.txt数量1.DataField = "数量";
            this.txt数量1.Height = 0.25F;
            this.txt数量1.Left = 2.2F;
            this.txt数量1.Name = "txt数量1";
            this.txt数量1.Style = "text-align: right; vertical-align: middle";
            this.txt数量1.Text = "txt数1";
            this.txt数量1.Top = 0F;
            this.txt数量1.Width = 0.5F;
            // 
            // checkBox1
            // 
            this.checkBox1.Height = 0.25F;
            this.checkBox1.Left = 3.064F;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Style = "";
            this.checkBox1.Text = "";
            this.checkBox1.Top = 0F;
            this.checkBox1.Width = 0.155F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7});
            this.pageHeader.Height = 0.8850001F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.5F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; vertical-align: middle";
            this.label1.Text = "2013年欧洲十国游物品整理清单";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // label2
            // 
            this.label2.Height = 0.25F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.01F;
            this.label2.Name = "label2";
            this.label2.Padding = new GrapeCity.ActiveReports.PaddingEx(16, 0, 0, 0);
            this.label2.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.label2.Text = "物品名";
            this.label2.Top = 0.6350001F;
            this.label2.Width = 2.2F;
            // 
            // label3
            // 
            this.label3.Height = 0.25F;
            this.label3.HyperLink = null;
            this.label3.Left = 2.21F;
            this.label3.Name = "label3";
            this.label3.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label3.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.label3.Text = "数量";
            this.label3.Top = 0.6350001F;
            this.label3.Width = 0.5F;
            // 
            // label4
            // 
            this.label4.Height = 0.25F;
            this.label4.HyperLink = null;
            this.label4.Left = 2.71F;
            this.label4.Name = "label4";
            this.label4.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label4.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.label4.Text = "已打包";
            this.label4.Top = 0.6350001F;
            this.label4.Width = 0.8F;
            // 
            // label5
            // 
            this.label5.Height = 0.25F;
            this.label5.HyperLink = null;
            this.label5.Left = 3.77F;
            this.label5.Name = "label5";
            this.label5.Padding = new GrapeCity.ActiveReports.PaddingEx(16, 0, 0, 0);
            this.label5.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: left; ver" +
    "tical-align: middle; ddo-char-set: 1";
            this.label5.Text = "物品名";
            this.label5.Top = 0.635F;
            this.label5.Width = 2.2F;
            // 
            // label6
            // 
            this.label6.Height = 0.25F;
            this.label6.HyperLink = null;
            this.label6.Left = 5.97F;
            this.label6.Name = "label6";
            this.label6.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label6.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.label6.Text = "数量";
            this.label6.Top = 0.6350001F;
            this.label6.Width = 0.5F;
            // 
            // label7
            // 
            this.label7.Height = 0.25F;
            this.label7.HyperLink = null;
            this.label7.Left = 6.47F;
            this.label7.Name = "label7";
            this.label7.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label7.Style = "background-color: #39A3C4; color: White; font-weight: bold; text-align: center; v" +
    "ertical-align: middle; ddo-char-set: 1";
            this.label7.Text = "已打包";
            this.label7.Top = 0.6350001F;
            this.label7.Width = 0.8F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.4F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.4F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // rptDownAcross
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "SELECT *   FROM 物品清单\r\nORDER BY 行号";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.27F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: RosyBrown", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #FFC0C0", "行间隔色", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt项目编号1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt项目编号2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量1;
        private GrapeCity.ActiveReports.SectionReportModel.CheckBox checkBox1;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
    }
}
