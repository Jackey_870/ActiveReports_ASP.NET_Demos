using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptPerformance.
    /// </summary>
    public partial class rptPerformance : GrapeCity.ActiveReports.SectionReport
    {

        public rptPerformance()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        private void rptPerformance_ReportStart(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Col1");

            for (int r = 1; r <= 20000; r++)
            {
                dt.Rows.Add(r);
            }
            this.DataSource = dt;
        }
    }
}
