using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptProductsByCategory.
    /// </summary>
    public partial class rptProductsByCategory : SectionReportBase
    {

        public rptProductsByCategory()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        float sub = 0;
        float sum = 0;

        private void detail_Format(object sender, EventArgs e)
        {
            float product = float.Parse(textBox1.Value.ToString());
            sub += product;
            sum += product;
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            textBox4.Value = sub.ToString();
            sub = 0;
        }

        private void rptProductsByCategory_ReportStart(object sender, EventArgs e)
        {

        }
    }
}
