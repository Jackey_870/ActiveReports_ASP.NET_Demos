using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptDownAcross.
    /// </summary>
    public partial class rptDownAcrossWithGroup : SectionReportBase
    {

        public rptDownAcrossWithGroup()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        int RowNo = 0;
        private void detail_Format(object sender, EventArgs e)
        {
            if (RowNo % 2 == 0)
            {
                this.shape1.BackColor = Color.Gainsboro;
            }
            else
            {
                this.shape1.BackColor = Color.Transparent;
            }

            RowNo++;
        }
    }
}
