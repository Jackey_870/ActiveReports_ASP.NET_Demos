﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="使用说明.aspx.cs" Inherits="ActiveReportsDemo.使用说明" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="font-family:微软雅黑;">
    <form id="form1" runat="server">
    <div style="width:800px;height:500px;margin-top:20px; margin-left:auto;margin-right:auto;">
    ActiveReports报表用户，您好！
     
        <br />
        <br />
        等你这么多久，我们终于在这里相遇，知道你急着揭开我的神秘面纱，不过在这之前，最好把我的使用方法看看清楚哦，哈哈！<br />
        <ol>
            <li style="margin-bottom:20px;"><b>我的功能：</b>分为报表功能展示和行业报表库两部分，报表功能主要介绍各种报表需求的实现方法，有完整的操作步骤；行业报表库中提供了大量的现成模板，你可以直接使用哦，同时，你也可以 <a href="http://gcdn.gcpowertools.com.cn/showtopic-17182.html">共享你开发的模板</a>，与广大报表开发者共同建设<b>史上第一个开源报表库项目</b>。</li>
            <li style="margin-bottom:20px;"><b>使用要求：</b>该工程使用的是 ActiveReports 9.3.4300.0 版本，如果你安装的不是该版本，可以参考 <a href="http://blog.gcpowertools.com.cn/post/2015/04/22/ActiveReportsUpgrade.aspx">ActiveReports的一键升级</a> 博客文章进行升级。</li>            
            <li style="margin-bottom:20px;"><b>修改数据源：</b>请打开Data目录下全部 .rdsx 文件，修改数据源地址，这样你就可以在设计时直接预览报表效果。</li>            
            <li style="margin-bottom:20px;"><b>学习交流：</b>你在使用该源码，或者报表开发过程中遇到的任何问题，都可以<a href="http://gcdn.gcpowertools.com.cn/showforum-41.html">向产品专家和一大波用户求助</a>，也可以将你掌握的技巧用于帮助他人。</li>
        </ol>
        怎么样，我的功能是不是很强大，使用方法还简单吧。如果你想和我在一起，那就通过电话联系我吧<b> 400-657-6008</b>。
        <br />
        
    </div>
    </form>
</body>
</html>
